# **Git-Flow**

Esta documentação servirá como guia, informando como realizar todos os passos-a-passos para utilizar o gitflow de maneira simples.

### **Versão**

0.0.1

## **Menu**

1. [O que é o Git-Flow?](#markdown-header-1-o-que-e-o-git-flow)

2. [Conceitos Básicos](#markdown-header-2-conceitos-basicos)  
    2.1 [Repositórios](#markdown-header-21-repositorios)  
    2.2 [Branchs](#markdown-header-22-branchs)  
    2.3 [Commits](#markdown-header-23-commits)  
    2.4 [Pull-requests](#markdown-header-24-pull-requests)  

3. [Convenções Adotadas](#markdown-header-3-convencoes-adotadas)  
  3.1 [Nomes das features para as **Branchs**](#markdown-header-31-nomes-das-features-para-as-branchs)  
  3.2 [Commit](#markdown-header-32-commit)

4. [Iniciando](#markdown-header-4-iniciando)  
  4.1 [Repositórios Iniciais](#markdown-header-41-repositorios-iniciais)  
  4.2 [Clonando os repositórios remotos](#markdown-header-42-clonando-os-repositorios-remotos)  
  4.3 [Manipulando branchs remotas para trabalho](#markdown-header-43-manipulando-branchs-remotas-para-trabalho)

5. [Iniciando](#markdown-header-5-finalizando)  
  5.1 [Pull-request aceitado](#markdown-header-51-pull-request-aceitado)  

----

## **1 - O que é o Git-Flow**

O git-flow é um conjunto de extensões para o git que provê operações de alto-nível para repositórios. Ele facilita o trabalho em equipe, assim como a organização de todo o código e histórico de cada feature, hotfix e releases.

----



## **2 - Conceitos Básicos**

Soon...



#### **2.1 - Repositórios**

Soon...



#### **2.2 - Branchs**

Soon...



#### **2.3 - Commits**

Soon...



#### **2.4 - Pull-requests**

Soon...

----



## **3- Convenções Adotadas**

Nessa seção será mostrada todas as convenções adotadas por **Calazans**.



#### **3.1 - Nomes das features para as Branchs**

Cada **branch** que irá ser criada tanto remotamente quanto localmente deverá ser escrita da seguinte forma:
  > 'TIPO#NUMERO_ISSUE'-MINI_DESCRIÇÃO_OPCIONAL

**Exemplo real:**
  > 'feature/#48'

ou
  > 'feature/#48-add-new-form-of-payment'

####**Atenção**

* Os *Tipos* podem ser três: **feature** (para novas features/funcionalidades), **hotfix** (para bugs/erros), **release** (para novas builds);
* Como mostrado nos exemplos acimas, a convenção é utilizar sempre descrições em **inglês** caso queira colocar a mesma;
* Nas próximas seções o *nome* das branchs está sendo refenciado como **NOME_FEATURE**, a qual é o exemplo acima.



#### **3.2 - Commit**

Para explicar e exemplificar melhor o **commit** vou separar em tópicos o *'corpo'* de um commit.

Todo **commit** deverá ser em **inglês** e seguir as convenções ditadas abaixo.

* Comando básico para dar o *commit* com **assinatura** do desenvolvedor:
  > git commit -sm "MESSAGEM"

A assinatura é o commando '-s' de *signature*.

* **Messagem** parte do *título* é a primeira linha para refenrenciar o que foi feito. O Título deverá conter no **máximo 50 caracteres**:
  > [NOME_FEATURE] - BREVE_TITULO_DO_QUE_FOI_FEITO

* **Messagem** parte do *corpo* é a partir da *terceira* linha para refenrenciar tudo o que foi feito, ou seja, após a **primeira** linha há uma quebra de linha em **branco**. A partir da terceira linha cada **linha** deverá conter no **máximo 70 caracteres**:
  > \- DESCRICAO_1;  
  > \- DESCRICAO_2;  
  > \- DESCRICAO_3.

* Veja agora um exemplo completo de um **COMMIT**:
  > git commit -sm "#48 Add new form of payment  
  > //ESSA LINHA DEVERÁ SER EM BRANCO (É A QUEBRA DE LINHA NECESSÁRIA)  
  > \- create new model of payment;  
  > \- create new test for this model;  
  > \- implement new methods into PaymentController."  

####**Observação**

**Não** faça apenas *um* commit no desenvolvimento de uma **feature**. Sempre quebre seus commits em **pedaços**, divindindo seu código em **responsablidades**. Isso **ajudará** a outros desenvolvedores entender melhor o que você fez e **facilitará** uma volta de algum ponto do commit caso haja necessidade.

Um ótimo comando para ajudar é **git add -p**. Irá ajudar a quebrar seus *commits* em pedaços.

----



## **4 - Iniciando**

Nessa seção será mostrado o passo-a-passo de como utilizar o git-flow.



#### **4.1 - Repositórios Iniciais**

Como convenção o repositório remoto deverá possuir duas branchs:

* master;
* develop;

**Branch Master**

O merge para o *master* será feito apenas quando finalizar um realese de produção.

**Branch Develop**

Esse é o branch que será realizado todo o processo de desenvolvimento até o realease ser feito. Todo pull-request das branchs features e hotfixs serão feito nessa brach.



#### **4.2 - Clonando os repositórios remotos**

Antes de iniciar o desenvolvimento, é necessário baixar o repositório para sua máquina. Basta digitar:
  > git clone URL_DO_REPOSITORIO_REMOTO

Quando clonar o projeto você terá a branch **master** por padrão. Será necessário criar uma branch **local** referente a branch **remota** *develop*. Digite:
  > git checkout -b develop origin/develop

Agora você está pronto para começar o desenvolvimento.



#### **4.3 - Manipulando branchs remotas para trabalho**

Cada desenvolvedor deverá manipular uma branch remota para cada **Issue** que irá iniciar o desenvolvimento.

**1 - Inciando uma nova *issue* **:

* Antes de qualquer procedimento, sempre que for criar uma nova branch baseada na **develop** atualize ela antes. Vá para a branch *develop* e atualize:
  > git checkout develop  
  > git pull origin develop

* Depois de atualizar a branch *develop*, crie uma nova *branch* local baseada na branch **develop** nomeando com o nome da feature:
  > git checkout -b NOME_FEATURE develop
* Com a branch local da feature criada, dê um push para criar essa mesma branch remotamente:
  > git push -u origin NOME_FEATURE

Após esse procedimento, basta iniciar a codificação da *issue* nessa branch.

**2 - Finalizando a *issue***:

* Após realizar todos os commits e fazer o *push* para o repositório remoto da **feature** está na hora de fezer o pull-request da branch *NOME_FEATURE* para branch *develop* via GitHub ou Bitbucket;  

* No GitHub ou Bitbucket o desenvolvedor irá selecionar a opção de criar um novo **pull-request** entre os repositórios, selecionando as *branchs*, para realizar o merge.

----


## **5 - Finalizando**



#### **5.1 - Pull-request aceitado**

Após seu pull-request ter sido aceitado por outro desenvolvedor e a **issue** ter ido para o board de **Ready to Approvall** está na hora de remover os **branchs** que você criou.

* Primeiro remova o **branch** remoto. Pode ser pelo próprio GitHub ou Bitbucket;
* Depois de remover lá, está na hora de limpar sua área de trabalho, removendo as branchs localmente:
  > git branch -d NOME_FEATURE

Após todos esses processos você está pronto para pegar outra **issue**.

----